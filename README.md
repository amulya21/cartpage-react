install node npm

clone the repository and run
 command - "npm install"

run command - "npm start" and app is served on "localhost:3000"

Our code goes into folder called "src"
 At start in src/main.js key called "defaultState" is set in localStorage and
 App's default state is stored in a file called src/defaultState.js in src folder
 So this way we get our default state from localStorage
 App follows redux architecture and store is created in createStore.js in src/store/createStore.js

 out global state includes 2 keys 
 	{ "products" : {}, "cart": {} }
 	1) products include 2 keys { items: [], itemsById : {} }
 		items includes ids which needs to be shown in order
 		itemsById stores the data of product with a particular id
 	2) cart includes object which includes key which is id of product 
 	    and stores  object { quantity: 4 } giving the quantity of added product in cart


 Our routes include "Home" route which is indexRoute in routes/index.js
 Home includes 
 	1) "container" called HomeViewContainer (src/routes/Home/containers/HomeViewContainer.js) which
 	passes mapDispatchToProps and mapStateToProps to "connect" imported from "react-redux" which passes
 	the keys in object returned buy mapStateToProps and mapStateToProps as props to component given as argument
 	in connect invocation and that component is HomeView.js (src/routes/Home/components/HomeView.js)
 		(*) mapStateToProps gives product object to HomeView as props
 		(*) mapDispatchToProps gives 4 actions to 
 		   		1) add product to cart 
 		   		2) increase quantity of a product (when "+" button is clicked from cart)
 		   		3) decrease quantity of a product (when "-" button is clicked from cart)
 		   		4) remove product (when "X" button is clicked from cart)

 	2) component called HomeView (src/routes/Home/components/HomeView.js) which incules component which 
 	is rendered and we can see this includes
 				1) HomeView component - renders Cart component and passes action props to <Cat/> and <Items/>
 				2) Cart component - renders the cart items and recieve actionsCallback to increase , decrease, remove item from cart
 				3) items component - shows the products that can be added to cart and receive a callback to add product 
 				to cart

 So redux includes actions, reducers
 	-- All actions types i.e the strings which tells action type goes in store/action.js
 	-- Actions which dispatches the action with an object with action type and action data goes in
 	file store store/dispatchAction.js
 	   1) actions (increaseQuantity, decreaseQuantity, removeFromCart) returns a simple object 
 	   	which is dispatched
 	#--------------------------------------THUNK MIDDLEWARE------------------------------------------------------
 	   2) action "addToCartAction" returns a CALLBACK so this shows an example of THUNK MIDDLEWARE 
 	   	Thunk catches that thing dispatched by addToCartAction is a callback so it gives 
 	   	dispatch , getState as inputs to this callback and runs this callback which in turn
 	   	dispatched an action object with actionType ans actionData 
 	#--------------------------------------THUNK MIDDLEWARE------------------------------------------------------


 	when actions are dispatched reducers are called and all reducers are in src/reducers it have
 	two reducers productReducer.js and cartReducer.js . All actions for the app are handled by cartReducers.js
 	    reducers check whether it has the functions corresponding to the dispatched action in object
 	    called actionHandler which include functions which gives a NEW STATE corresponding to an action type.
 	    So if action type is there in actionHandler that function is called by which new state is returned and
 	    store is updated and App rerenders accordingly.



