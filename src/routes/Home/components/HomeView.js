import React from 'react'
import './HomeView.scss'

export class Items extends React.Component {

	addToTheCart = (itemId) => {
		let { addToCart } = this.props;
		addToCart(itemId);
	}

	render () {
		let { items, itemsById } = this.props;
		return (
			<div className="col-md-8">
				<div className="containerOuter containerOuter-prd"> PRODUCTS </div>
				{ items.map((itemId) => (
					<div className="col-md-5 product-container" style={{ display: "inline-block" }} key = {itemId}>
						<img className="product-pic" src={itemsById[itemId].src} />
					 	<div className="product-name">{itemsById[itemId].name} ({itemsById[itemId].price}) </div>
					 	<div className="btn product-add" onClick={() => { this.addToTheCart(itemId) }}> Add to cart </div>
					 </div>
					)) }
			</div>
		);
	}
}

export const Cart = ({ cart, itemsById, increaseQuantity, decreaseQuantity, removeFromCart }) => {
	return (
		<div className="col-md-4">
			<div className="containerOuter containerOuter-cart" >CART</div>
			<div className="row text-uppercase" style={{marginBottom: "15px"}}>
				<div className="col-md-4 heading">items</div>
				<div className="col-md-3 heading" style={{ textAlign: "center"}}>quantity</div>
			</div>
			<div>
				{(() => {
					let cartView = [];
					for (let id in cart) {
						cartView.push(<div className="row" key={id}>
							<div className="col-md-4 cart-item">
								<img className="cart-pic" src={itemsById[id].src} />
						 		<div className="cart-name">{itemsById[id].name} </div>
							</div>
							<div className="col-md-3" style={{ textAlign: "center"}}>{cart[id].quantity}</div>
							<div className="col-md-5">
								<button className="btn btn-default btn-sm operation"
									onClick={ () => { increaseQuantity(id) } }>+</button>
								<button className="btn btn-default btn-sm operation"
									onClick = { () => { decreaseQuantity(id) } } 
									style = {{ cursor: `${cart[id].quantity == 0 ? "no-drop": "pointer" }`}}
									disabled={cart[id].quantity == 0}>-</button>
								<button className="btn btn-default btn-sm operation"
									onClick = { () => { removeFromCart(id) } } >X</button>
							</div>  
						 </div>)
					}
					return cartView.length ? cartView : <div className="emptyCart">ADD SOMETHING TO CART</div>;
				})()}
			</div>
		</div>
	)
}

export class HomeView extends React.Component {
	constructor (props) {
		super(props);
	}
	render () {
		let { cart, products : {items, itemsById}, addToCart,
		increaseQuantity, decreaseQuantity, removeFromCart } = this.props;
		return (
			<div className="row">
				<Items items={items} itemsById={itemsById} addToCart={addToCart}/>
				<Cart cart={cart} itemsById={itemsById} increaseQuantity={increaseQuantity} 
					decreaseQuantity={decreaseQuantity} removeFromCart={removeFromCart} />
			</div>
		)
	}
}

export default HomeView
