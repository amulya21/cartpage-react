import React from "react";
import { connect } from "react-redux";
import HomeView from "./../components/HomeView";
import { addToCartAction, increaseQuantity, decreaseQuantity, removeFromCart } from "./../../../store/dispatchActions"

const mapStateToProps = (state, ownProps) => {
	let { products, cart } = state;
	return {
		products: products,
		cart: cart,
	}
} 
const mapDispatchToProps =  (dispatch) => {
	return {
		addToCart : (itemId) => dispatch(addToCartAction(itemId)),
		increaseQuantity: (itemId) => dispatch(increaseQuantity(itemId)),
		decreaseQuantity: (itemId) => dispatch(decreaseQuantity(itemId)),
		removeFromCart : (itemId) => dispatch(removeFromCart(itemId))
	}
}

export default connect(mapStateToProps, mapDispatchToProps) (HomeView);