import {
	ADD_TO_CART,
	INCREASE_QUANTITY,
	DECREASE_QUANTITY,
	REMOVE_FROM_CART
}  from "./actions"

// thunk
export const addToCartAction = (itemId) => (dispatch, getState) => {
	dispatch({
		type: ADD_TO_CART,
		data : {
			itemId: itemId
		}
	});
}

export const increaseQuantity = (itemId) => ({
		type: INCREASE_QUANTITY,
		data: { itemId: itemId } 
	})
export const decreaseQuantity = (itemId) => ({
	type: DECREASE_QUANTITY,
	data: { itemId: itemId}
});
export const removeFromCart = (itemId) => ({
	type: REMOVE_FROM_CART,
	data: { itemId : itemId }
});
