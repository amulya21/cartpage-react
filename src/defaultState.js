export default {
	products : {
		items : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ,16, 17],
		itemsById : {
			0 : { name : "item A", price : "$23", src: "https://b.wadicdn.com/product/sa/94/7531/1-product.jpg" }, 
			1 : { name : "item B", price : "$24", src: "https://b.wadicdn.com/product/sa/14/7902/1-product.jpg" }, 
			2 : { name : "item C", price : "$25", src : "https://b.wadicdn.com/product/sa/46/0/1-product.jpg" }, 
			3 : { name : "item D", price : "$26", src : "https://b.wadicdn.com/product/sa/21/3431/1-product.jpg" }, 
			4 : { name : "item E", price : "$27", src : "https://b.wadicdn.com/product/sa/01/9071/1-product.jpg" }, 
			5 : { name : "item F", price : "$28", src : "https://b.wadicdn.com/product/sa/23/822/1-product.jpg" }, 
			6 : { name : "item G", price : "$29", src : "https://b.wadicdn.com/product/sa/01/1022/1-product.jpg" }, 
			7 : { name : "item H", price : "$11", src: "https://b.wadicdn.com/product/41/3732/1-product.jpg" }, 
			8 : { name : "item I", price : "$34", src : "https://b.wadicdn.com/product/sa/61/643/1-product.jpg" }, 
			9 : { name : "item J", price : "$54", src : "https://b.wadicdn.com/product/sa/77/462/1-product.jpg" },
			10 : { name : "item K", price : "$54", src : "//b.wadicdn.com/product/ae/67/8164/1-product.jpg" },
			11 : { name : "item L", price : "$54", src : "//b.wadicdn.com/product/59/6391/1-product.jpg" },
			12 : { name : "item M", price : "$54", src : "//b.wadicdn.com/product/sa/61/9204/1-product.jpg" },
			13 : { name : "item N", price : "$54", src : "//b.wadicdn.com/product/sa/85/09/1-product.jpg" },
			14 : { name : "item O", price : "$54", src : "https://images-eu.ssl-images-amazon.com/images/I/51uPEbFR5HL._AC_SY400_.jpg" },
			15 : { name : "item P", price : "$54", src : "https://b.wadicdn.com/product/41/3732/1-product.jpg" },
			16 : { name : "item Q", price : "$54", src : "https://b.wadicdn.com/product/sa/46/0/1-product.jpg" },
			17 : { name : "item R", price : "$54", src : "https://b.wadicdn.com/product/sa/23/822/1-product.jpg" },

		}
	},
	cart : {
		0 : { quantity: 3 },
		1 : { quantity: 2 }
	}
};